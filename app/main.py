from typing import List

from crud import (
    create_db,
    create_recipe,
    db_data,
    read_ingredients,
    read_recipe_detail,
    read_recipes,
)
from database import DATABASE_URL, Base, async_session, engine, get_async_session
from fastapi import Depends, FastAPI, HTTPException
from models import Ingredient, Recipe
from schemas import IngredientOut, RecipeDetail, RecipeIn, RecipeOut
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy_utils import database_exists

app = FastAPI()


@app.on_event("startup")
async def startup():
    if not database_exists(DATABASE_URL):
        async with engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)
            async with async_session() as session:
                await create_db(session, db_data)


@app.on_event("shutdown")
async def shutdown():
    # await session.close()
    await engine.dispose()


@app.get("/")
def root():
    return {"message": "Wellcome To Our Cookbook"}


@app.post("/recipes/", response_model=RecipeOut, status_code=201)
async def recipes(
    recipe: RecipeIn, session: AsyncSession = Depends(get_async_session)
) -> Recipe:
    """Эндпойнт для добавления рецепта в БД"""
    data = recipe.dict()
    return await create_recipe(session, data)


@app.get("/recipes/", response_model=List[RecipeOut])
async def get_recipes(
    session: AsyncSession = Depends(get_async_session),
) -> List[Recipe]:
    """Эндпойнт для получения списка рецептов из БД"""
    return await read_recipes(session)


@app.get("/ingredients/", response_model=List[IngredientOut])
async def ingredients(
    session: AsyncSession = Depends(get_async_session),
) -> List[Ingredient]:
    """Эндпойнт для получения списка ингридиентов из БД"""
    return await read_ingredients(session)


@app.get("/recipes/{recipe_id}", response_model=RecipeDetail)
async def recipe_detail(
    recipe_id: int, session: AsyncSession = Depends(get_async_session)
) -> Recipe:
    """Эндпойнт для получения детальной информации по рецепту по его id"""
    result = await read_recipe_detail(session, recipe_id)
    if not result:
        raise HTTPException(status_code=404, detail="Recipe not found")
    return result
