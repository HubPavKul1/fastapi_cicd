from typing import List, Optional

from models import Ingredient, Recipe, RecipeIngredient
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select
from sqlalchemy.orm import selectinload

db_data = [
    {
        "title": "щи",
        "cooking_time": 80,
        "view_count": 0,
        "ingredients": [
            {"name": "мясо"},
            {"name": "капуста"},
            {"name": "вода"},
            {"name": "картофель"},
        ],
        "description": "очень вкусно",
    },
    {
        "title": "бутерброд",
        "cooking_time": 10,
        "view_count": 0,
        "ingredients": [
            {"name": "хлеб"},
            {"name": "масло сливочное"},
            {"name": "сыр"},
        ],
        "description": "приятного аппетита",
    },
]


async def create_db(session: AsyncSession, data: List[dict]) -> None:
    """Заполняем базу данных"""
    for item in data:
        await create_recipe(session, item)


async def read_recipes(session: AsyncSession, limit: int = 10) -> List[Recipe]:
    """Получаем список рецептов, отсортированных по
    количеству просмотров и времени приготовления"""
    results = await session.execute(
        select(Recipe)
        .order_by(Recipe.view_count.desc(), Recipe.cooking_time)
        .limit(limit)
    )
    return list(results.scalars().all())


async def read_ingredients(session: AsyncSession) -> List[Ingredient]:
    """Получаем список ингредиентов,
    отсортированных по наименованию"""
    results = await session.execute(select(Ingredient).order_by(Ingredient.name))
    return list(results.scalars().all())


async def create_recipe(session: AsyncSession, data: dict) -> Recipe:
    """Добавляем новый рецепт в базу данных"""
    ingredients_data = data.pop("ingredients")
    new_recipe = Recipe(**data)
    session.add(new_recipe)
    await session.commit()
    await session.refresh(new_recipe)

    await create_ingredients(session, ingredients_data)
    await create_relation_recipe_ingredients(
        session,
        ingredients_data,
        new_recipe.id,
    )
    return new_recipe


async def create_ingredients(
    session: AsyncSession, data_list: List[dict[str, str]]
) -> None:
    """Добавляем ингредиенты в базу данных"""
    ingredients_data_to_create = [
        item["name"]
        for item in data_list
        if not await get_ingredient_id_by_name(session, item["name"])
    ]
    new_ingredients = [Ingredient(name=item) for item in ingredients_data_to_create]  # type: ignore
    session.add_all(new_ingredients)
    await session.commit()


async def create_relation_recipe_ingredients(
    session: AsyncSession, ingredients: List[dict[str, str]], recipe_id: int
) -> None:
    """Создаем связь many-to-many рецепта и ингредиентов"""
    ingredients_id = [
        await get_ingredient_id_by_name(session, ingredient["name"])
        for ingredient in ingredients
    ]
    if ingredients_id:
        new_recipe_ingredients = [
            RecipeIngredient(recipe_id=recipe_id, ingredient_id=item_id)  # type: ignore
            for item_id in ingredients_id
        ]
        session.add_all(new_recipe_ingredients)
        await session.commit()


async def read_recipe_detail(session: AsyncSession, recipe_id: int) -> Optional[Recipe]:
    """Получаем рецепт из базы данных по его id"""
    query = await session.execute(
        select(Recipe)
        .where(Recipe.id == recipe_id)
        .options(selectinload(Recipe.ingredients))
    )
    recipe = query.scalar()
    if recipe:
        recipe.view_count += 1
        session.add(recipe)
        await session.commit()
    return recipe


async def read_ingredient_detail(
    session: AsyncSession, ingredient_id: int
) -> Optional[Ingredient]:
    """Получаем ингредиент из базы данных по его id"""
    query = await session.execute(
        select(Ingredient).where(Ingredient.id == ingredient_id)
    )
    return query.scalar()


async def get_ingredient_id_by_name(session: AsyncSession, name: str) -> Optional[int]:
    """Получаем ингредиент из базы данных по его наименованию"""
    result = await session.execute(select(Ingredient.id).where(Ingredient.name == name))
    return result.scalar()
