import pytest
from crud import read_recipes
from httpx import AsyncClient

from .conftest import async_session, test_data_invalid, test_data_valid


@pytest.mark.asyncio
async def test_root(ac: AsyncClient):
    response = await ac.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Wellcome To Our Cookbook"}


@pytest.mark.asyncio
async def test_post_recipes(ac: AsyncClient, prepare_db):
    async with async_session() as session:
        recipes = await read_recipes(session)
        assert len(recipes) == 0
        response = await ac.post("/recipes/", json=test_data_valid)
        assert response.status_code == 201
        recipes = await read_recipes(session)
        assert len(recipes) == 1
        response = await ac.get("/recipes/1")
        title = response.json().get("title")
        assert title == "рыбный суп"


@pytest.mark.asyncio
async def test_post_recipes_invalid(ac: AsyncClient, prepare_db):
    response = await ac.post("/recipes/", json=test_data_invalid)
    assert response.status_code == 422


@pytest.mark.asyncio
async def test_get_recipes(ac: AsyncClient, prepare_db):
    response = await ac.get("/recipes/")
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_get_ingredients(prepare_db, ac: AsyncClient):
    response = await ac.get("/ingredients/")
    assert response.status_code == 200
