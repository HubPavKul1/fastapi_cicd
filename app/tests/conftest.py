from typing import AsyncGenerator

import pytest_asyncio
from database import Base, get_async_session
from httpx import AsyncClient
from main import app
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine

DATABASE_URL_TEST = "sqlite+aiosqlite://"

engine_test = create_async_engine(DATABASE_URL_TEST, echo=True)
async_session = async_sessionmaker(
    engine_test, expire_on_commit=False, class_=AsyncSession
)


test_data_valid = {
    "title": "рыбный суп",
    "cooking_time": 80,
    "view_count": 0,
    "ingredients": [
        {"name": "рыба"},
        {"name": "лук"},
        {"name": "вода"},
        {"name": "картофель"},
    ],
    "description": "очень вкусно",
}

test_data_invalid = {
    "title": "щи",
    "cooking_time": "wrong_value",
    "view_count": 0,
    "ingredients": [
        {"name": "мясо"},
        {"name": "капуста"},
        {"name": "вода"},
        {"name": "картофель"},
    ],
    "description": "очень вкусно",
}


async def override_get_async_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session() as session:
        yield session


app.dependency_overrides[get_async_session] = override_get_async_session


@pytest_asyncio.fixture
async def prepare_db():
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)
    yield
    async with engine_test.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


@pytest_asyncio.fixture
async def ac() -> AsyncGenerator[AsyncClient, None]:
    async with AsyncClient(app=app, base_url="http://test") as ac:
        yield ac
