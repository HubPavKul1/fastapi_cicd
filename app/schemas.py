from typing import Optional

from pydantic import BaseModel


class BaseIngredient(BaseModel):
    name: str


class IngredientIn(BaseIngredient):
    pass


class IngredientOut(BaseIngredient):
    id: int

    class Config:
        orm_mode = True


class BaseRecipe(BaseModel):
    title: str
    cooking_time: int
    view_count: int = 0


class RecipeIn(BaseRecipe):
    ingredients: list[IngredientIn]
    description: Optional[str] = None


class RecipeOut(BaseRecipe):
    id: int

    class Config:
        orm_mode = True


class RecipeDetail(RecipeOut):
    ingredients: list[IngredientOut]
    description: Optional[str] = None

    class Config:
        orm_mode = True
