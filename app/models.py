from typing import List, Optional

from database import Base
from sqlalchemy import ForeignKey, String
from sqlalchemy.orm import Mapped, mapped_column, relationship


class Ingredient(Base):
    """Модель Ингредиент"""

    __tablename__ = "ingredients"

    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(200))
    recipes: Mapped[List["Recipe"]] = relationship(
        secondary="recipe_ingredient",
        back_populates="ingredients",
        cascade="all, delete",
    )

    def __repr__(self):
        return f"Ингредиент: {self.name}"


class Recipe(Base):
    """Модель Рецепт"""

    __tablename__ = "recipes"

    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[str] = mapped_column(String(200))
    cooking_time: Mapped[int]
    view_count: Mapped[int] = mapped_column(default=0)
    ingredients: Mapped[List["Ingredient"]] = relationship(
        secondary="recipe_ingredient",
        back_populates="recipes",
        cascade="all, delete",
    )
    description: Mapped[Optional[str]]

    def __repr__(self):
        return f"Рецепт: {self.title}"


class RecipeIngredient(Base):
    """Модель для связи рецепта и ингредиентов по типу many-to-many"""

    __tablename__ = "recipe_ingredient"

    recipe_id: Mapped[int] = mapped_column(ForeignKey("recipes.id"), primary_key=True)
    ingredient_id: Mapped[int] = mapped_column(
        ForeignKey("ingredients.id"), primary_key=True
    )
