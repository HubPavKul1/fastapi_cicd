venv:
	python3 -m virtualenv -p python3.9 venv

deps:
	python3 -m pip install --upgrade pip
	pip install -r requirements.txt

test:
	pytest

lint:
	black --check --diff app/
	isort --check-only --profile black app/
	flake8 app/
	mypy app/

lint_fix:
	black app/
	isort --profile black app/

check:
	make test
	make lint
